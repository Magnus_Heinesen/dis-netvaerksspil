package game2020;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class Server {

	private static final int portNumber = 6789;
	private int serverPort;
	private static List<ServerThread> clients;


	public static void main(String[] args) throws Exception {
		Server server = new Server(portNumber);
		server.startServer();
	}

	public Server(int portNumber) {
		this.serverPort = portNumber;
	}

	public List<ServerThread> getClients() {
		return clients;
	}

	private void startServer() {
		clients = new ArrayList<ServerThread>();
		ServerSocket serverSocket = null;
		try {
			serverSocket = new ServerSocket(serverPort);
			acceptClients(serverSocket);
		} catch (IOException e) {
			System.err.println("Could not listen on port: " + serverPort);
			System.exit(1);
		}

	}

	private void acceptClients(ServerSocket serverSocket) {

		System.out.println("Server started on port = " + serverSocket.getLocalSocketAddress());
		while (true) {
			try {
				System.out.println("Ready to accept client connection...");
				Socket socket = serverSocket.accept();
				System.out.println("Accepted connection from: " + socket.getRemoteSocketAddress());
				ServerThread client = new ServerThread(socket);
				Thread thread = new Thread(client);
				client.start();
				clients.add(client);
			} catch (IOException e) {
				System.out.println("Accept failed on : " + serverPort);
			}
		}
	}
	
	public static synchronized void updateClients (String m) {
		for(ServerThread t : clients) {
			try {
				t.getOutToClient().writeBytes(m + "\n");
				System.out.println(m);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
