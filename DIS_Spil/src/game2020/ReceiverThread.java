package game2020;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class ReceiverThread extends Thread {

	private Socket connectionSocket;
	private BufferedReader inFromServer;

	public ReceiverThread(Socket socket) {
		connectionSocket = socket;

	}

	@Override
	public void run() {
		try {
			inFromServer = new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));
			while (true) {
				try {
					String ServerSentence = inFromServer.readLine();
					System.out.println(ServerSentence);
					Runthis.receiveServerMessage(ServerSentence);

				} catch (IOException e) {
					e.printStackTrace();
					break;
				}
			}
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}

}
