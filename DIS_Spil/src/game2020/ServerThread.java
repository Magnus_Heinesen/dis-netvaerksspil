package game2020;

import java.net.*;
import java.io.*;

public class ServerThread extends Thread {
	private Socket connSocket;
	private DataOutputStream outToClient;
	private BufferedReader inFromClient;

	public ServerThread(Socket connSocket) {
		this.connSocket = connSocket;
	}
	
	

	public DataOutputStream getOutToClient() {
		return outToClient;
	}

	public void run() {

		try {
			outToClient = new DataOutputStream(connSocket.getOutputStream());
			inFromClient = new BufferedReader(new InputStreamReader(connSocket.getInputStream()));

			while (true) {
				try {
					String clientSentence = inFromClient.readLine();

					//outToClient.write(clientSentence.getBytes());
					Server.updateClients(clientSentence);
					//System.out.println(clientSentence);

				} catch (IOException e) {
					e.printStackTrace();
					break;
				}
			}
		} catch (IOException e1) {
			e1.printStackTrace();
		}

	}

}
